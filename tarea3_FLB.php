<html lang = "español">
    <head>
        <title> Expresiones Regulares </title>
    </head>
    <body>
        <h1 align='center'> Funciones que validan expreiones regulares </h1>
        <h2 align='center'> Para validar emails, curps, palabras y números, llama la función correspondiente e imprimela </h2>

        <?php

        function validar_email($email){
            $result = preg_match('/^[A-z0-9\\._-]+@[A-z0-9][A-z0-9-]*(\\.[A-z0-9_-]+)*\\.([A-z]{2,6})$/i', $email);
            return $result;
        }

        function validar_curps($curp){
            $result = preg_match('/^[A-Za-z]{6}[0-9]{8}[H,M][0-9]{3})$/i', $curp);
            return $result;
        }

        function validar_palabras($palabra){
            $result = preg_match('/^[A_Za-z]{50, }$/i', $palabra);
            return $result;
        }

        function no_simbolos($palabra){
            $result = preg_quote($palabra);
            return $result;
        }

        function detectar_decimales($num){
            $result = preg_match('/^[0-9]*[.]{1}[0-9]*$/i', $num);
            return $result;
        }

        $prueba = 454345.8473;
        $valido = detectar_decimales($prueba);
        echo "$prueba tiene decimales : $valido";

        ?>

    </body>
</html>